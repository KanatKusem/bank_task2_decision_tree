
# coding: utf-8

# In[101]:


import numpy as np
import pandas as pd
from sklearn.cross_validation import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn import tree
import xlsxwriter


# In[3]:


cd desktop


# In[4]:


xl = pd.ExcelFile("task2.xlsx")


# In[5]:


df = xl.parse("Мошенники_train")


# In[6]:


df.fillna(0, inplace=True)


# In[7]:


def handle_non_numerical_data (df):
    columns = df.columns.values
    
    for column in columns:
        text_digit_vals = {}
        def convert_to_int(val):
            return text_digit_vals[val]
        
        if df[column].dtype != np.int64 and df[column].dtype !=np.float64:
            column_contents = df[column].values.tolist()
            unique_elements = set(column_contents)
            x = 0
            for unique in unique_elements:
                if unique not in text_digit_vals:
                    text_digit_vals[unique] = x
                    x+=1
                    
            df[column] = list(map(convert_to_int, df[column]))
            
    return df

df_clean = handle_non_numerical_data(df)
df_clean.head()


# In[8]:


df.head()


# In[59]:


df_clean.shape


# In[71]:


len(df_clean.columns)


# In[ ]:


df_clean.


# In[73]:


X = df_clean.loc[:, "NUM":"F129"]


# In[74]:



Y = df_clean.loc[:,"TARGET"]


# In[75]:


X.shape


# In[90]:


X_train, X_test, y_train, y_test = train_test_split( X, Y, test_size = 0.3, random_state = 100)


# In[76]:


df_clean.describe()


# In[41]:


df_clean.info()


# In[42]:


df_clean.isnull().any()


# In[44]:


clf_gini = DecisionTreeClassifier(criterion = "gini", random_state = 100,
                               max_depth=3, min_samples_leaf=5)


# In[93]:


clf_gini.fit(X_train, y_train)


# In[94]:


y_pred = clf_gini.predict(X_test)
y_pred


# In[95]:


accuracy_score(y_test,y_pred)*100


# In[79]:


test_data = xl.parse("Мошенники_test")


# In[80]:


test_data.shape


# In[81]:


def handle_non_numerical_data_test (test_data):
    columns = test_data.columns.values
    
    for column in columns:
        text_digit_vals = {}
        def convert_to_int(val):
            return text_digit_vals[val]
        
        if test_data[column].dtype != np.int64 and test_data[column].dtype !=np.float64:
            column_contents = test_data[column].values.tolist()
            unique_elements = set(column_contents)
            x = 0
            for unique in unique_elements:
                if unique not in text_digit_vals:
                    text_digit_vals[unique] = x
                    x+=1
                    
            test_data[column] = list(map(convert_to_int, test_data[column]))
            
    return test_data

test_data_clean = handle_non_numerical_data_test(test_data)
test_data_clean.head()


# In[82]:


test_data_clean.describe()


# In[83]:


test_data_clean.fillna(0, inplace=True)


# In[84]:


test_data_clean.isnull().any()


# In[85]:


test_data_clean.info()


# In[86]:


df.info()


# In[91]:


X_train.shape


# In[88]:


df_clean.shape


# In[89]:


test_data_clean.shape


# In[96]:


test_pred = clf_gini.predict(test_data_clean)
test_pred


# In[97]:


test_data_clean['TARGET'] = test_pred


# In[99]:


test_data_clean.head(50)


# In[100]:


test_data_clean.shape


# In[114]:


writer = pd.ExcelWriter('task2.xlsx', engine='xlsxwriter')


# In[115]:


test_data_clean.to_excel(writer, sheet_name='Sheet2')

